LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
	src/rt-app.c \
	src/rt-app_args.c \
	src/rt-app_parse_config.c \
	src/rt-app_utils.c

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/libdl \
	$(LOCAL_PATH)/json-c-android

LOCAL_STATIC_LIBRARIES := \
	json-c

LOCAL_MODULE := rt-app
include $(BUILD_EXECUTABLE)

